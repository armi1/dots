. /etc/ksh.kshrc

HISTFILE="$HOME/.kshhistory"
HISTSIZE=5000

PS1="[\u@\h \w]$ "

alias ls="eza -lAh --group-directories-first"
alias ll="eza -lh --group-directories-first"
alias la="eza -Ah --group-directories-first"
