;; adding melpa to the repos
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; setting up use-package
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

;; enabling evil mode + some extra keybinds for it (i don't really like vim keys i might disable this)
;;    (use-package evil
;;    :ensure t
;;    :init
;;    (setq evil-want-keybinding nil)
;;    (setq evil-vsplit-window-right t)
;;    (setq evil-split-window-velow t)
;;    (evil-mode))
;;
;;    (use-package evil-collection
;;    :after evil
;;    :ensure t
;;    :config
;;    (evil-collection-init))

(load-file "~/.emacs.d/config.el")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("3c101ce5144083a775ef0cbf0c33b0744c11000659c73d4af5f1498b94e4426c" "f60058c11b788dcb3c0c5e687cdf45da075b6740ecf645e29b49e1239cc0fb6c" default))
 '(package-selected-packages '(nerd-icons general which-key evil-collection)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
