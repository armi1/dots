;;; monochrome-theme.el --- monochrome
;;; Version: 1.0
;;; Commentary:
;;; A theme called monochrome
;;; Code:

(deftheme monochrome "DOCSTRING for monochrome")
  (custom-theme-set-faces 'monochrome
   '(default ((t (:foreground "#ffffff" :background "#000000" ))))
   '(cursor ((t (:background "#ffffff" ))))
   '(fringe ((t (:background "#000000" ))))
   '(mode-line ((t (:foreground "#ffffff" :background "#000000" ))))
   '(region ((t (:background "#3e3e3e" ))))
   '(secondary-selection ((t (:background "#1a1a19" ))))
   '(font-lock-builtin-face ((t (:foreground "#4d4d4d" ))))
   '(font-lock-comment-face ((t (:foreground "#545454" ))))
   '(font-lock-function-name-face ((t (:foreground "#ffffff" ))))
   '(font-lock-keyword-face ((t (:foreground "#b2b2b2" ))))
   '(font-lock-string-face ((t (:foreground "#ffffff" ))))
   '(font-lock-type-face ((t (:foreground "#ffffff" ))))
   '(font-lock-constant-face ((t (:foreground "#707070" ))))
   '(font-lock-variable-name-face ((t (:foreground "#ffffff" ))))
   '(minibuffer-prompt ((t (:foreground "#ffffff" :bold t ))))
   '(font-lock-warning-face ((t (:foreground "ffffff" :bold t ))))
   )

;;;###autoload
(and load-file-name
    (boundp 'custom-theme-load-path)
    (add-to-list 'custom-theme-load-path
                 (file-name-as-directory
                  (file-name-directory load-file-name))))
;; Automatically add this theme to the load path

(provide-theme 'monochrome)

;;; monochrome-theme.el ends here
