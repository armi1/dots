;; setting up themes
(load "~/.emacs.d/monochrome-theme.el")
(load-theme 'monochrome t)

;; setting up the font
(set-face-attribute 'default nil
		    :font "Terminess Nerd Font Mono 8"
		    :weight 'medium)
(set-face-attribute 'variable-pitch nil
		    :font "Ubuntu Nerd Font 8"
		    :weight 'medium)
(set-face-attribute 'fixed-pitch nil
		    :font "Terminess Nerd Font Mono 8"
		    :weight 'medium)

;; apparently without this fonts can be rly weird and small? idk i didn't look into it
(add-to-list 'default-frame-alist '(font . "Terminess Nerd Font Mono 8"))

;; setting random stuff
(setq c-basic-offset 8
      tab-width 8
      make-backup-files nil)

(setq-default indent-tabs-mode nil)

(setopt display-fill-column-indicator-column 80)
(add-hook 'prog-mode-hook #'display-fill-column-indicator-mode)

;; rust mode
(use-package rust-mode
  :ensure t)

(require 'rust-mode)

;; Keybindings
(use-package general
  :ensure t)
(load "~/.emacs.d/keybinds.el")

;; whichkey
(use-package which-key
  :init (which-key-mode 1)
  :diminish
  :config
  (setq which-key-side-window-location 'bottom
	which-key-allow-imprecise-window-fit nil
	which-key-sort-order #'which-key-key-order-alpha
	which-key-sort-uppercase-first nil
	which-key-column-padding 1
	which-key-max-display-columns nil
	which-key-min-display-lines 6
	which-key-side-window-slot -10
	which-key-side-window-max-height 0.25
	which-key-idle-delay 0.8
	which-key-max-description-length 25
	which-key-allow-imprecise-window-fit nil
	which-key-separator " > " ))

;; removing the silly toolbars n whatnot
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; line numbers and truncating lines
(global-display-line-numbers-mode 1)
(global-visual-line-mode t)

;; project support
(use-package projectile
  :ensure t)
(projectile-mode +1)

;; icon support
(use-package all-the-icons
  :ensure t
  :if (display-graphic-p))

;; dashboard
(use-package dashboard
  :ensure t
  :init
  (setq initial-buffer-choice 'dashboard-open
	dashboard-set-heading-icons nil
	dashboard-set-file-icons t
	dashboard-icon-type 'all-the-icons
	dashboard-banner-logo-title "welcome <3"
	dashboard-startup-banner "~/.emacs.d/dashboard.txt"
	dashboard-center-content t
	dashboard-footer-messages '("productivity is a myth.")
	dashboard-items '((recents . 5)
			  (agenda . 5))
	dashboard-navigator-buttons `(((,
					(all-the-icons-faicon "cog" :height 0.7 :v-adjust 0.0)
				      "config"
				      "open emacs config"
				      (lambda (&rest _) (interactive) (find-file "~/.emacs.d/config.el")))
				       (,(all-the-icons-faicon "calendar" :height 0.7 :v-adjust 0.0)
				       "agenda"
				       "open org agenda"
				       (lambda (&rest _) (org-agenda)))))
	dashboard-startupify-list '(dashboard-insert-banner
				    dashboard-insert-newline
				    dashboard-insert-banner-title
				    dashboard-insert-navigator
				    dashboard-insert-items
				    dashboard-insert-newline
				    dashboard-insert-footer))
  :config
  (dashboard-setup-startup-hook))

;; hiding modeling in dashboard
(add-hook 'dashboard-mode-hook (lambda () (setq-local mode-line-format nil)))

;; installing ivy (and counsel)
(use-package counsel
  :ensure t
  :after ivy
  :config (counsel-mode))

(use-package ivy
  :bind
  (("C-c C-r" . ivy-resume)
   ("C-x B" . ivy-switch-buffer-other-window))
  :custom
  (setq ivy-use-virtual-buffers t
	ivy-count-format "(%d/%d) "
	enable-recursive-minibuffers t)
  :config
  (ivy-mode))

(use-package all-the-icons-ivy-rich
  :ensure t
  :init (all-the-icons-ivy-rich-mode 1))

(use-package ivy-rich
  :after ivy
  :ensure t
  :init (ivy-rich-mode 1)
  :custom
  (ivy-virtual-abbreviate 'full
   ivy-rich-switch-buffer-align-virtual-buffer t
   ivy-rich-path-style 'abbrev)
  :config
  (ivy-set-display-transformer 'ivy-switch-buffer
			       'ivy-rich-switch-buffer-transformer))


;; configuring ERC
(setq erc-prompt (lambda () (concat "[ERC on " (buffer-name) "]")))

;; == ORG MODE ==

;; org agenda
(require 'org)
(setq org-agenda-files '("~/org/agenda.org"))

(use-package toc-org
  :commands toc-org-enable
  :ensure t
  :init (add-hook 'org-mode-hook 'toc-org-mode))

(add-hook 'org-mode-hook 'org-indent-mode)
(use-package org-bullets
  :ensure t)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
