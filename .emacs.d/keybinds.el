(require 'general)

;; making C-c the leader key
(general-create-definer armi/leader-keys
  :prefix "C-c")

;; general keybinds
(armi/leader-keys
  "s" '(save-buffer :wk "save buffer")
  "r" '((lambda () (interactive) (load-file "~/.emacs.d/init.el")) :wk "reload config")
  "i" '(ibuffer :wk "ibuffer"))

;; opening dired
(armi/leader-keys
  "d" '(:ignore t :wk "dired")
  "d d" '(dired   :wk "open dired")
  "d e" '((lambda () (interactive) (dired "~/.emacs.d/"))
	          :wk "open dired in emacs directory"))

;; file interaction
(armi/leader-keys
  "f" '(:ignore t         :wk"files")
  "f c" '((lambda () (interactive) (find-file "~/.emacs.d/config.el"))
	                  :wk "open config")
  "f ." '(find-file       :wk "find file")
  "f r" '(counsel-recentf :wk "find recent files"))

;; evaluation (i like never evaluate elisp but who cares?
(armi/leader-keys
  "e" '(:ignore t          :wk "evaluate")
  "e b" '(eval-buffer     :wk "evaluate buffer")
  "e e" '(eval-expression :wk "evaluate expression")
  "e r" '(eval-region     :wk "evaluate region"))

;; wahhh help me im a lil baby :(
(armi/leader-keys
  "h" '(:ignore t            :wk "help")
  "h f" '(describe-function  :wk "describe function")
  "h v" '(describe-variable  :wk "describe variable")
  "h t" '(help-with-tutorial :wk "emacs tutorial")) ;; like once a month i deadass forget every keybind and have to check this LOL

;; windowing because emacs is actually appalling without any changes
(armi/leader-keys
  "w" '(:ignore t            :wk "windows")
  "w k" '(quit-window        :wk "kill window")
  "w b" '(split-window-below :wk "split below")
  "w r" '(split-window-right :wk "split right")
  "w w" '(other-window       :wk "switch window"))
  
;; buffers
(armi/leader-keys
  "b" '(:ignore t :wk "buffers")
  "b f" '(switch-to-buffer :wk "find and switch buffer")
  "b k" '(kill-this-buffer :wk "kill buffer")
  "b n" '(next-buffer      :wk "next buffer")
  "b p" '(previous-buffer  :wk "previous buffer")
  "b r" '(revert-buffer    :wk "reload buffer"))
